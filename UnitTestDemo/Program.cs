﻿using System;
using UnitTestDemo.Classes;

namespace UnitTestProbe
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var config = new ConfigProvider();
                var resultado=MainProcess.Execute(args,config);
                Console.WriteLine("Suma es " + resultado.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Environment.Exit(1);
            }
        }
    }
}
