﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTestProbe
{
    public class Sumador
    {
        public dynamic Suma(string param1,string param2)
        {
            dynamic resultado;
            int number1, number2;

            try
            {
                int.TryParse(param1, out number1);
                int.TryParse(param2, out number2);
                resultado = number1 + number2;
                
            } catch (Exception ex)
            {
                throw;
            }
            return resultado;
        }
    }
}

