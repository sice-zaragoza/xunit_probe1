﻿using System;
using System.Collections.Generic;
using System.Text;
using UnitTestDemo.Classes;

namespace UnitTestProbe
{
    public class Validador
    {

        public static void ValidarArrayParametros(string[] parameters)
        {
            if (parameters.Length < 2) { throw new ArgumentException("Requiere por lo menos dos números"); }
        }
        public static void ValidarParametro(string param,string param_name, ConfigProvider config)
        {
            if (param.Length > 10 ) { throw new OverflowException("Parameter "+param_name+" has more than 10 digits"); }
            int number;
            var r = int.TryParse(param, out number);
            if (string.IsNullOrWhiteSpace(param)==true)
            {
                throw new ArgumentNullException("Param  " + param_name + " parece un espacio vacio");
            }
            if (r == false)
            {
                throw new FormatException("Param  " + param_name + " no parece un número");
            }
            var allow_negative = config.Get("allow_negative");
            if (number < 0 && allow_negative==false)
            {
                throw new FormatException("Param  " + param_name + " no puede ser negativo");
            }
        }
    }
}
