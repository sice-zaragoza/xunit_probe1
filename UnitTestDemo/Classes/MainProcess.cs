﻿using System;
using UnitTestDemo.Classes;

namespace UnitTestProbe
{
    public class MainProcess
    {
        public static dynamic Execute(string[] args,ConfigProvider config=null)
        {
                if (config==null) { config = new ConfigProvider(); }
                Validador.ValidarArrayParametros(args);
                                
                Validador.ValidarParametro(args[0],"Param1",config);
                Validador.ValidarParametro(args[1], "Param2",config);

                var sumador = new Sumador();
                var suma = sumador.Suma(args[0], args[1]);
                            
                return suma;
        }
    }
}
