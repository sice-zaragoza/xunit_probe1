﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTestDemo.Classes
{
    public class ConfigProvider
    {
        private Dictionary<string, bool> Data = new Dictionary<string, bool>
        {
            {"aloow_negative", true }
        };

        public virtual bool Get(string key)
        {
            bool value = Data.GetValueOrDefault(key);
            return value;
        }
    }
}
