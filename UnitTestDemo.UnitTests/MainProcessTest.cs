using Moq;
using System;
using UnitTestDemo.Classes;
using Xunit;

namespace UnitTestProbe.UnitTests
{
    public class MainProcessTest
    {
        [Fact]
        [Trait("Category", "MainProcess")]
        public void When_ParamIsTooBig_Expect_Exception()
        {
            var big_number = "1231231232132313213";
            Action act = () => MainProcess.Execute(new string[] { "1", big_number });
            Assert.Throws<OverflowException>(act);
        }

        [Fact]
        [Trait("Category", "MainProcess")]
        public void When_ParamIsEmptySpace_Expect_Exception()
        {
            string space = " ";
            Action act = () => MainProcess.Execute(new string[] { "1", space });
            Assert.Throws<ArgumentNullException>(act);
        }

        [Fact]
        [Trait("Category", "MainProcess")]
        public void When_ParamIsOnlyOne_Expect_Exception()
        {
            Action act = () => MainProcess.Execute(new string[] { "1" });
            Assert.Throws<ArgumentException>(act);
        }

        [Fact]
        [Trait("Category", "MainProcess")]
        public void When_NoParams_Expect_Exception()
        {
            Action act = () => MainProcess.Execute(new string[] { });
            Assert.Throws<ArgumentException>(act);
        }

        [Theory]
        [InlineData("uno", "dos")]
        [InlineData("tres", "cuatro")]
        [InlineData("cinco", "seis")]
        [Trait("Category", "MainProcess")]
        public void When_ParamIsNotANumber_Expect_Exception(params string[] args)
        {
            Action act = () => MainProcess.Execute(args);
            Assert.Throws<FormatException>(act);
        }

        [Fact]
        [Trait("Category", "MainProcess")]
        public void When_NegativeNotAllowed_Expect_Exception()
        {
            var mock = new Mock<ConfigProvider>();
            mock.Setup(foo => foo.Get("allow_negative")).Returns(false);
            Action act = () => MainProcess.Execute(new string[] {"-1","2" },mock.Object);
            Assert.Throws<FormatException>(act);
        }

        [Fact]
        [Trait("Category", "MainProcess")]
        public void When_NegativeAllowed_Expect_Ok()
        {
            var mock = new Mock<ConfigProvider>();
            mock.Setup(foo => foo.Get("allow_negative")).Returns(true);
            MainProcess.Execute(new string[] { "-1", "2" }, mock.Object);
            //no exception is ok test by default
        }

        [Fact]
        [Trait("Category", "MainProcess")]
        public void When_NegativeAllowed_Expect_Returned_Int32Type()
        {
            var mock = new Mock<ConfigProvider>();
            mock.Setup(foo => foo.Get("allow_negative")).Returns(true);
            var resultado = MainProcess.Execute(new string[] { "-1", "2" }, mock.Object);
            var type = resultado.GetType().Name;
            Assert.True(type.Equals("Int32"));
        }
    }
}
