using Moq;
using System;
using UnitTestDemo.Classes;
using Xunit;

namespace UnitTestProbe.UnitTests
{
    public class SumadorTest
    {
        [Fact]
        [Trait("Category", "Sumador")]
        public void When_AddingTwoNumbers_Expect_Int32()
        {
            var sumador = new Sumador();
            var resultado = sumador.Suma("1", "2");
            var type = resultado.GetType().Name;
            Assert.True(type.Equals("Int32"));
        }



    }
}
